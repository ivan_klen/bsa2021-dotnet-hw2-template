﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using CoolParking.BL.Models;
using System.Collections.Generic;

public static class Settings
{
    static int InitialBalance { get; set; }
    static int Capacity { get; set; }
    static int PaymentWriteOffPeriod { get; set; }
    static int LoggingPeriod { get; set; }

    static Dictionary<VehicleType, double> Tariffs = new Dictionary<VehicleType, double>();

    static double PenaltyFactor { get; set; }
}