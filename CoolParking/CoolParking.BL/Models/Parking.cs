﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public Collection<Vehicle> Vehicles { get; set; }
        public int Balance { get; set; }
    }
}